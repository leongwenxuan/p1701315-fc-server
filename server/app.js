var path          = require('path');
var express       = require ('express');
var bodyParser    = require('body-parser');
// var userCtrl      = require('./api/user/user.controller');
var foodcourtCtrl = require('./api/foodcourt/foodcourt.controller');
var stallCtrl = require('./api/stall/stall.controller');
var dishesCtrl = require('./api/dishes/dishes.controller');
var updatesCtrl = require('./api/updates/updates.controller');
var contactusCtrl = require('./api/contactus/contactus.controller');


var app  = express();
var cors = require('cors');

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, "../public")));

app.use(cors());

app.get("/api/foodcourt", foodcourtCtrl.list);
// app.post("/api/foodcourt", foodcourtCtrl.add);

app.get("/api/stall", stallCtrl.list);
// app.post("/api/stall", stallCtrl.add);

app.get("/api/dishes", dishesCtrl.list);

app.get("/api/updates", updatesCtrl.list);

app.post("/api/contactmain", contactusCtrl.create);
app.get("/api/contactmain", contactusCtrl.list);



// app.get("/api/user", userCtrl.list);
// app.post("/api/user", userCtrl.create)

// Catch all
app.use(function (req, resp) {
    resp.status(440);
    resp.send("Error File not Found");
});

// set port and start webserver
app.listen('3000', function () {
    console.log("Server running at http://localhost:3000");
});
    