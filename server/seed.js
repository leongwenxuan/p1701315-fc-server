var configDB = require("./configDB");
var database = require("./database");

var apiURI = "http://localhost:3000";

var Foodcourt = database.Foodcourt;
var Stalls = database.Stalls;
var Dishes = database.Dishes;
var Updates = database.Updates;
var Contactus = database.ContactUs;

module.exports = function () {
    if (configDB.seed) {
        Foodcourt.bulkCreate([
            {
                fc_id: 1, fc_name: "FoodCourt 1", location: "DESIGN SCHOOL BLOCK", op_hours: "Mon-Sat 8AM-8PM",
                fc_img01: apiURI + "/Images/fc1.jpg", fc_img02: apiURI + "/Images/fc2.jpg",
                description: "Wow Awesome Great Stuff Here"
            },
            {
                fc_id: 2, fc_name: "FoodCourt 2", location: "BESIDE DESIGN", op_hours: "Mon-Sat 8AM-8PM",
                fc_img01: apiURI + "/Images/fc3.jpg", fc_img02: apiURI + "/Images/fc4.jpg",
                description: "Jezuz The Drinks Are The Best"
            },
            {
                fc_id: 3, fc_name: "FoodCourt 3", location: "NEAR ABE", op_hours: "Mon-Sat 8AM-8PM",
                fc_img01: apiURI + "/Images/fc4.jpg", fc_img02: apiURI + "/Images/fc5.jpg",
                description: "Mac And Cheese"
            }
        ])
            .then(function () {
                console.log("Done creating Foodcourt records");

                Stalls.bulkCreate([
                    {
                        st_id: 1, fc_id: 1, st_name: "Vegetarian Stall", st_img01: apiURI + "/Images/stall1.jpg",
                        description: "Tasty Veggies",
                    },
                    {
                        st_id: 2, fc_id: 1, st_name: "Chicken Rice Stall", st_img01: apiURI + "/images/stall2.jpg",
                        description: "Chicken Taste Very Tender",

                    },
                    {
                        st_id: 3, fc_id: 1, st_name: "Indian Stall", st_img01: apiURI + "/Images/stall3.JPG",
                        description: "Cheap Pratas",
                    },
                    {
                        st_id: 4, fc_id: 2, st_name: "Japanese Stall", st_img01: apiURI + "/Images/stall4.jpg",
                        description: "Ramen Fusion",
                    },
                    {
                        st_id: 5, fc_id: 2, st_name: "Malay Stall", st_img01: apiURI + "/Images/stall5.jpg",
                        description: "Taste Of Malaysia",
                    },
                    {
                        st_id: 6, fc_id: 2, st_name: "Yong Tau Foo Stall", st_img01: apiURI + "/Images/stall6.JPG",
                        description: "",
                    },
                    {
                        st_id: 7, fc_id: 3, st_name: "Boost", st_img01: apiURI + "/Images/stall1.jpg",
                        description: "Drink Stall With The Freshest Fruits",
                    },
                    {
                        st_id: 8, fc_id: 3, st_name: "Treasure House", st_img01: apiURI + "/Images/stall2.jpg",
                        description: "Freshest Tea",
                    },
                    {
                        st_id: 9, fc_id: 3, st_name: "Cantonese Stall", st_img01: apiURI + "/Images/stall3.JPG",
                        description: "Lastest Cantonese Dishes On The Menu",
                    }
                ]).then(function () {
                    console.log("Done creating Stalls Records");

                    Dishes.bulkCreate([
                        {
                            d_id: 1, st_id: 1, d_name: "Chicken Rice", description: "Deep Fried Chicken Rice",
                            price: 2.50, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes1.jpg"
                        },
                        {
                            d_id: 2, st_id: 1, d_name: "Ramen", description: "Spring Noodles With A Good Broth",
                            price: 5.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes2.jpg"
                        },
                        {
                            d_id: 3, st_id: 1, d_name: "Steak", description: "Medium Rare",
                            price: 40, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/images/dishes3.jpg"
                        },
                        {
                            d_id: 4, st_id: 2, d_name: "Mash Potato", description: "Fresh Potatos",
                            price: 3, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes3.jpg"
                        },
                        {
                            d_id: 5, st_id: 2, d_name: "Burger", description: "Most Delicious Burgers",
                            price: 7.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes4.jpg"
                        },
                        {
                            d_id: 6, st_id: 2, d_name: "Chicken Nugget", description: "Cheap and Crunchy Nuggets",
                            price: 2, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/images/dishes5.jpeg"
                        },
                        {
                            d_id: 7, st_id: 3, d_name: "CheeseCake", description: "Creamy Cake",
                            price: 20, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes6.jpg"
                        },
                        {
                            d_id: 8, st_id: 3, d_name: "Mushroom Pasta", description: "Best fragance Pasta",
                            price: 10, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes7.jpg"
                        },
                        {
                            d_id: 9, st_id: 3, d_name: "BlueBerry Cake", description: "Freshly Baked",
                            price: 15, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/images/dishes1.jpg"
                        },
                        {
                            d_id: 10, st_id: 4, d_name: "Steamed Buns", description: "Hot Buns",
                            price: 3, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes2.jpg"
                        },
                        {
                            d_id: 11, st_id: 4, d_name: "Noodle Soup", description: "Soft Noodles",
                            price: 3.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes3.jpg"
                        },
                        {
                            d_id: 12, st_id: 4, d_name: "Pudding", description: "Fresh Jelly",
                            price: 6.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes4.jpg"
                        },
                        {
                            d_id: 13, st_id: 5, d_name: "Water", description: "Plain Refreshing Water",
                            price: 5.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes5.jpg"
                        },
                        {
                            d_id: 14, st_id: 5, d_name: "Wine", description: "Aged Wine",
                            price: 6.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes6.PNG"
                        },
                        {
                            d_id: 15, st_id: 5, d_name: "Cheesy Chicken", description: "Korean Cheesy Chicken",
                            price: 4.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/images/dishes7.jpg"
                        },
                        {
                            d_id: 16, st_id: 6, d_name: "Prawn Rolls", description: "Spicy Rolls",
                            price: 3.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/images/dishes1.jpg"
                        },
                        {
                            d_id: 17, st_id: 6, d_name: "Chips", description: "Fresh Potato Deep Fried Chips",
                            price: 6.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes2.jpg"
                        },
                        {
                            d_id: 18, st_id: 6, d_name: "Jelly", description: "Chill Jelly With Milk",
                            price: 2.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes3.jpg"
                        },
                        {
                            d_id: 19, st_id: 7, d_name: "Pork Bun", description: "Fresh Pork",
                            price: 8.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes4.jpg"
                        },
                        {
                            d_id: 20, st_id: 7, d_name: "Steam Soup", description: "Fragrance Soup",
                            price: 3.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes5.jpg"
                        },
                        {
                            d_id: 21, st_id: 7, d_name: "Mushroom Soup", description: "Shrooms Soup",
                            price: 6.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes6.jpg"
                        },
                        {
                            d_id: 22, st_id: 8, d_name: "Cream Cake", description: "",
                            price: 5.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes7.jpg"
                        },
                        {
                            d_id: 23, st_id: 8, d_name: "Omelette", description: "",
                            price: 6.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes1.PNG"
                        },
                        {
                            d_id: 24, st_id: 8, d_name: "Soft Tofu", description: "Silky Tofu",
                            price: 4.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/images/dishes2.jpg"
                        },
                        {
                            d_id: 25, st_id: 9, d_name: "Bandung", description: "Pink Sweet",
                            price: 3.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/images/dishes3.jpg"
                        },
                        {
                            d_id: 26, st_id: 9, d_name: "Espresso", description: "Strong Coffee",
                            price: 6.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes4.jpg"
                        },
                        {
                            d_id: 27, st_id: 9, d_name: "IceCream", description: "Chilled jelly with shredded coconut in coconut milk",
                            price: 2.90, availability: "Mon-Sat 8AM-8PM",
                            d_img01: apiURI + "/Images/dishes5.jpg"
                        }
                    ]).then(function () {
                        console.log("Done creating dishes records");

                        Updates.bulkCreate([
                            {
                                updates_id: 1, updates_title: "New Food", description: "May 6 2018",
                                description: 'Stall 1 has a new menu'
                            },
                            {
                                updates_id: 2, updates_title: "Maintainence", description: "7 August 2018",
                                description: 'FoodCourt 5 will be close for infestation inspectiond'
                            }
                        ]).then(function () {
                            console.log("Done creating News records");

                            Contactus.bulkCreate([
                                {
                                    contact_id: 1, name: "Daniel", email: "Daniel@gmail.com", subject: "Drinks", foodcourtNo: "Foodcourt 4",
                                    message: "Nice Drinks"
                                }
                            ]).then(function () {
                                console.log("Done creating Feedback records");
                            });
                        });
                    });
                });
            });
    }
}