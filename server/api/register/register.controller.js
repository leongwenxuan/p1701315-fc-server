var configDB = require("../../configDB");
var Register = require("../../database").Register;

var   bcrypt     = require('bcrypt');
const saltRounds = 10;

exports.list = function (req, resp) {
    Register
        .findAll({  // select * from users
            attributes:['register_id','user_n']
        }
        ).then(function (result) {
            resp
                .status(200)
                .type("application/json")
                .json(result);
        }).catch(function (err) {
            resp.status(500).json({ error: true });
        });
}

exports.create = function (req, resp) {
    if (!req.body.info) {
        resp.status(400).json({ error: true });
    } else {
        var newInfo = req.body.info;

        bcrypt.hash(newInfo.user_pwd, saltRounds)
            .then(function (hash) {
                // Store hash in your password DB.
                newInfo.user_pwd = hash;
                Register
                    .create(newInfo)
                    .then(function (newRecord) {
                        delete newRecord.dataValues.user_pwd
                        resp
                            .status(200)
                            .type("application/json")
                            .json(newRecord);
                    })
                    .catch(function (err) {
                        resp.status(500).json({ error: true });
                    });
            })
            .catch(function (err) {
                resp.status(500).json({ error: true });
            });
    }
}