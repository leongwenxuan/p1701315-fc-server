
// var StallList = [
//     {stall_id: 1, fc_id: 1,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 2, fc_id: 1,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 4, fc_id: 1,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 5, fc_id: 1,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 6, fc_id: 1,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},

//     {stall_id: 1, fc_id: 2,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 2, fc_id: 2,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 3, fc_id: 2,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 4, fc_id: 2,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 5, fc_id: 2,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 6, fc_id: 2,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},

//     {stall_id: 1, fc_id: 3,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 2, fc_id: 3,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 3, fc_id: 3,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 4, fc_id: 3,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 5, fc_id: 3,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."},
//     {stall_id: 6, fc_id: 3,stall_name: "Ramen" , location: "DESIGN SCHOOL BLOCK",op_hours: "Mon-Sat 8AM-8PM",stall_img01: "fc1.jpg",description: "Food is really nice."}
//   ];

var configDB = require("../../configDB");
var Stalls = require("../../database").Stalls;

exports.list = function(req, resp){

    var whereClauseFC = (req.params.fc_id) ? {fc_id: parseInt(req.params.fc_id)} : {};

    Stalls
        .findAll({
            where:whereClauseFC
        }).then(function(result){
            resp
                .status(200)
                .type("application/json")
                .json(result);
        }).catch(function(err){
            res.status(500).json({error: true})
        });
}
