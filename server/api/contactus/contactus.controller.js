var configDB = require("../../configDB");
var Feedback = require("../../database").Feedback;

exports.list = function(req,resp){
    Feedback
        .findAll(

        ).then(function (result){
            resp
                .status(200)
                .type("application/json")
                .json(result);
        }).catch(function(err){
            handleErr(resp,err);
        });
}

exports.create = function (req,resp){
    if(!req.body.info){
        resp.status(400).json({error:true});
    }else{
        var newInfo = req.body.info;
        
        Feedback
            .create(newInfo) 
            .then(function(){
                resp
                .status(200)
                .type("application/json")
                .json(newInfo);
            })
            .catch(function(err){
                handleErr(resp, err);
            })

    }
}
// Error handling 
function handleErr(res) {
    handleErr(res, null);
}

function handleErr(res, err) {
    console.log(err);
    res.status(500).json({error: true});
}