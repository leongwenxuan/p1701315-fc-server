var configDB = require("../../configDB");
var Dishes = require("../../database").Dishes;

exports.list = function (req,resp){

    var whereClauseSt = (req.params.st_id) ? {st_id: parseInt(req.params.st_id)} : {};

    Dishes
        .findAll(
            {
                where:whereClauseSt
            }).then(function(result){
                resp 
                    .status(200)
                    .type("application/json")
                    .json(result);
            }).catch(function(err){
                res.status(500).json({error: true})
            });
}