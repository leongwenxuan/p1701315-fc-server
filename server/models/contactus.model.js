var Sequelize = require("sequelize");

module.exports = function (database){
    return database.define(
        'contactmain', {
            contact_id:{
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            name:{
                type: Sequelize.STRING(100),
                primaryKey: false,
                allowNull: false
            },
            email:{
                type:Sequelize.STRING(100),
                primaryKey: false,
                allowNull: false
            },
            subject:{
                type:Sequelize.STRING(100),
                primaryKey: false,
                allowNull: false
            },
            foodcourtNo:{
                type:Sequelize.STRING(100),
                primaryKey: false,
                allowNull: false
            },
            message:{
                type:Sequelize.STRING(100),
                primaryKey: false,
                allowNull: false
            }
        },
        {
            tableName:'contactmain',
            timestamps: false
        }
    );
}