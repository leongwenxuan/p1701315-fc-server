var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define(
        'Registers', {
        register_id : {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        user_n : {
            type: Sequelize.STRING(255),
            primaryKey: false,
            allowNull: false
        },
        user_login : { //email
            type: Sequelize.STRING(255),
            primaryKey: false,
            allowNull: false
        },
        user_num : {
            type: Sequelize.INTEGER,
            primaryKey: false,
            allowNull: false
        },
        usr_pwd : {
            type: Sequelize.STRING(255),
            primaryKey: false,
            allowNull: false
        }
    }, {
        tableName: 'Registers',
        timestamps: false
    });
}