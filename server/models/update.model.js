var Sequelize = require("sequelize");

module.exports = function (database){
    return database.define(
        'news', {
            updates_id:{
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            updates_title:{
                type: Sequelize.STRING(100),
                primaryKey: false,
                allowNull: false
            },
            description:{
                type:Sequelize.STRING(100),
                primaryKey: false,
                allowNull: false
            }
        },
        {
            tableName:'news',
            timestamps: false
        }
    );
}