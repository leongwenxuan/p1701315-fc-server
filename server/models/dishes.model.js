var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define(
        'dishes',
        {
            st_id: {
                type: Sequelize.INTEGER,
                primaryKey: false,
                allowNull: false,
                references: {
                    model: 'stalls',
                    key: 'st_id'
                }
            },
            d_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            d_name: {
                type: Sequelize.STRING(50),
                primaryKey: false,
                allowNull: false
            },
            d_img01: {
                type: Sequelize.STRING(1000),
                primaryKey: false,
                allowNull: false
            },
            description: {
                type: Sequelize.STRING(1000),
                primaryKey: false,
                allowNull: false
            },
            price: {
                type: Sequelize.DECIMAL(10,2),
                primaryKey: false,
                allowNull: false
            },
            availability: {
                type: Sequelize.STRING(50),
                primaryKey: false,
                allowNull: false
            },
        },
        {
            tableName: 'dishes',
            timestamps: false
        }
    );
}