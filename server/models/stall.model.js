var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define(
        'stalls',
        {
            
            fc_id: {
                type: Sequelize.INTEGER,
                primaryKey: false,
                allowNull: false,
                references: {
                    model: 'foodcourts',
                    key: 'fc_id'
                }
            },
            st_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            st_name: {
                type: Sequelize.STRING(200),
                primaryKey: false,
                allowNull: false
            },
            st_img01: {
                type: Sequelize.STRING(200),
                primaryKey: false,
                allowNull: false
            },
            description: {
                type: Sequelize.STRING(200),
                primaryKey: false,
                allowNull: false
            }
        },
        {
            tableName: "stalls",
            timestamps: false
        }
    );
}