var Sequelize = require('sequelize');
var configDB = require("./configDB");
var database;

database = new Sequelize(
    configDB.mysql.database,
    configDB.mysql.username,
    configDB.mysql.password, {
        host: configDB.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });

// include the models

// var Friend = require("./models/friend.model.js")(database);

//  Sync the database

var Foodcourt = require("./models/foodcourt.model.js")(database);
var Stalls = require("./models/stall.model.js")(database);
var Dishes = require("./models/dishes.model.js")(database);
var ContactUs = require("./models/contactus.model.js")(database);
var Updates = require("./models/update.model.js")(database);
var Register = require("./models/register.model.js")(database);

Foodcourt.hasMany(Stalls, {
    foreignKey: 'fc_id'
}); Stalls.hasMany(Dishes, {
    foreignKey: 'st_id'
});

database
    .sync({
        force: configDB.seed
    })
    .then(function () {
        console.log("Database in Sync Now");
        require("./seed")();
    });

// Expose Models

module.exports = {
    Foodcourt: Foodcourt,
    Stalls: Stalls,
    Dishes: Dishes,
    ContactUs: ContactUs,
    Updates: Updates,
    Register : Register
}
